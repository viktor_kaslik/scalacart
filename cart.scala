import scala.collection.mutable.HashMap


val prices: HashMap[String, Double] = HashMap(("Apple"->0.60), ("Orange"->0.25))
val discounts: HashMap[String, Int] = HashMap(("Apple"->2), ("Orange"->3))


def cart(items: List[String]): Double = {
  var quant: HashMap[String, Int] = HashMap()
  for(item <- items){
    if(quant.contains(item)){
      quant += (item -> (quant(item)+1))
    }else{
      quant += (item -> 1)
    }
  }
  var total = 0.0
  for(item <- quant.keys){
    //total = (quant(item)*prices(item)) + total
    total =  applyDiscount(item,quant(item)) + total
  }
  total
}

def applyDiscount(item: String, quantity: Int): Double = {
  var numberOfItemsToSub = quantity/discounts(item)
  var numToPrice = (quantity-numberOfItemsToSub)
  
  (math floor (numToPrice * prices(item))*100) /100
}



println(cart(List("Apple","Apple","Orange")))




