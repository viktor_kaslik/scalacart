# I have made 2 commits (part1 and part 2)

# Unfortunatly I have been unable to write any tests as my mac is having issues with installing scalaTest (xcode developer suite clashes)

# How I would have tested part 1
- I would have written a 2 tests to test normal opperation
    - ["Apple", "Apple", "Orrange"] and check that this returned 1.45
    - ["Apple", "Orrange"] and check that this returned 1.20
- I would have then writen another 3 (one passing in an empty list, one with invalid products, one with a large number of products)

## Part 2
- Now the discounts had been added I would expect my old tests to fail where any discounts had been met
- I would  then tweak the existing tests so that they met the new criteria and added a few more valid scenarions (various quantities) being passed in and ensured that they all passed

# Assumptions that I have made for correct functionality
- I have assumed that the items start with a Caps letter (A,O)
- I have assumed that a database was not expexted due to this being a small toy example, However, with an example this small I would have used a CSV to read in my items,price,offers
